import React, { useEffect, useState } from 'react';
import axios from 'axios';

const TodoList = () => {
    const [todoItems, setTodoItems] = useState([]);
    const [showAddForm, setShowAddForm] = useState(false);
    const [addedTask, setAddedTask] = useState({ title: '', description: '' });
    // const [editingTask, setEditingTask] = useState(null);

    useEffect(() => {
        fetchTodoItems();
    }, []);

    const fetchTodoItems = async () => {
        try {
            const response = await axios.get('http://localhost:5000/todoitems');
            const updatedTodoItems = response.data.map((task) => ({ ...task, editing: false }));
            setTodoItems(updatedTodoItems);
        } catch (error) {
            console.error('Error fetching todo items:', error);
        }
    };

    const toggleAddForm = () => {
        setShowAddForm(!showAddForm);
        setAddedTask({ title: '', description: '' });
    };

    const handleAddInputChange = (event) => {
        console.log(event);
        const { name, value } = event.target;
        setAddedTask({ ...addedTask, [name]: value });
    };

    const addNewTask = async (event) => {
        event.preventDefault();
        try {
            const response = await axios.post('http://localhost:5000/todoitems', addedTask);
            const newTask = response.data;
            setTodoItems([...todoItems, newTask]);
            setAddedTask({ title: '', description: '' }); // Reset the addedTask state
            setShowAddForm(false);
        } catch (error) {
            console.error('Error adding new task:', error);
        }
    };

    const handleEditInputChange = (taskId, name, value) => {
        setTodoItems((prevTodoItems) =>
            prevTodoItems.map((task) => {
                if (task.id === taskId) {
                    return { ...task, [name]: name === 'completed' ? !task.completed : value };
                }
                return task;
            })
        );
    };

    const toggleEdit = (taskId) => {
        setTodoItems((prevTodoItems) =>
            prevTodoItems.map((task) => {
                if (task.id === taskId) {
                    return { ...task, editing: !task.editing };
                }
                return task;
            })
        );
    };


    const updateTask = async (task) => {
        try {
            const response = await axios.put(`http://localhost:5000/todoitems/${task.id}`, task);
            const updatedTask = response.data;
            const updatedTodoItems = todoItems.map((item) => {
                if (item.id === task.id) {
                    return { ...updatedTask, editing: false };
                }
                return item;
            });
            setTodoItems(updatedTodoItems);
        } catch (error) {
            console.error('Error updating task:', error);
        }
    };

    const cancelEdit = (task) => {
        const updatedTodoItems = todoItems.map((item) => {
            if (item.id === task.id) {
                return { ...item, editing: false };
            }
            return item;
        });
        setTodoItems(updatedTodoItems);
    };

    const deleteTask = async (taskId) => {
        try {
            await axios.delete(`http://localhost:5000/todoitems/${taskId}`);
            // Update the state to remove the deleted task
            setTodoItems(todoItems.filter((task) => task.id !== taskId));
        } catch (error) {
            console.error('Error deleting task:', error);
        }
    };

    return (
        <div>
            <h1>Todo List</h1>
            <button className="btn btn-success" onClick={toggleAddForm}>Add New Task</button>

            {showAddForm &&
                <form onSubmit={addNewTask}>
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input
                            type="text"
                            className="form-control"
                            id="title"
                            name="title"
                            value={addedTask.title}
                            onChange={handleAddInputChange}
                            required
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <textarea
                            className="form-control"
                            id="description"
                            name="description"
                            value={addedTask.description}
                            onChange={handleAddInputChange}
                            required
                        ></textarea>
                    </div>
                    <br />
                    <button type="submit" className="btn btn-success">
                        Save
                    </button> &nbsp;
                    <button type="button" className="btn btn-secondary" onClick={toggleAddForm}>
                        Cancel
                    </button>
                </form>
            }

            <br />
            <br />

            <table className="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Completed</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {todoItems.map((task) => (
                        <tr key={task.id}>
                            <td>{task.id}</td>

                            {!task.editing && <td>{task.title}</td>}
                            {task.editing && (
                                <td>
                                    <input
                                        type="text"
                                        value={task.title}
                                        onChange={(event) => handleEditInputChange(task.id, 'title', event.target.value)}
                                    />
                                </td>
                            )}


                            {!task.editing && <td>{task.description}</td>}
                            {task.editing &&
                                <td>
                                    <input
                                        type="text"
                                        value={task.description}
                                        onChange={(event) => handleEditInputChange(task.id, 'description', event.target.value)}
                                    />
                                </td>
                            }

                            {/* {!task.editing && <td>{task.completed ? 'Yes' : 'No'}</td>} */}
                            {!task.editing &&
                                <td>
                                    <input
                                        type="checkbox"
                                        checked={task.completed}
                                        disabled
                                    />
                                </td>
                            }

                            {task.editing &&
                                <td>
                                    <input
                                        type="checkbox"
                                        checked={task.completed}
                                        onChange={(event) => handleEditInputChange(task.id, 'completed')}
                                    />
                                </td>
                            }


                            <td>
                                {!task.editing && <button className="btn btn-primary" onClick={() => toggleEdit(task.id)}>
                                    Edit
                                </button>}
                                {task.editing &&
                                    (<React.Fragment>
                                        <button className="btn btn-success" onClick={() => updateTask(task)}>
                                            Save
                                        </button> &nbsp;
                                        <button className="btn btn-secondary" onClick={() => cancelEdit(task)}>
                                            Cancel
                                        </button>
                                    </React.Fragment>)}
                                <button className="delete-btn" onClick={() => deleteTask(task.id)}>
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default TodoList;
