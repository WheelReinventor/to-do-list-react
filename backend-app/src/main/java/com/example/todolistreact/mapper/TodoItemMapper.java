package com.example.todolistreact.mapper;

import org.springframework.stereotype.Component;

import com.example.todolistreact.dto.TodoItemDTO;
import com.example.todolistreact.entity.TodoItem;

@Component
public class TodoItemMapper {

    public TodoItemDTO toDTO(TodoItem todoItem) {
        TodoItemDTO todoItemDTO = new TodoItemDTO();
        todoItemDTO.setId(todoItem.getId());
        todoItemDTO.setTitle(todoItem.getTitle());
        todoItemDTO.setDescription(todoItem.getDescription());
        todoItemDTO.setCompleted(todoItem.isCompleted());
        return todoItemDTO;
    }

    public TodoItem toEntity(TodoItemDTO todoItemDTO) {
        TodoItem todoItem = new TodoItem();
        todoItem.setId(todoItemDTO.getId());
        todoItem.setTitle(todoItemDTO.getTitle());
        todoItem.setDescription(todoItemDTO.getDescription());
        todoItem.setCompleted(todoItemDTO.isCompleted());
        return todoItem;
    }
}
