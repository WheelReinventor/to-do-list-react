package com.example.todolistreact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToDoListReactApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToDoListReactApplication.class, args);
	}

}
