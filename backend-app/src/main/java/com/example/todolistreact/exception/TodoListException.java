package com.example.todolistreact.exception;

public class TodoListException extends RuntimeException {
	
	 private static final long serialVersionUID = 1L;

	public TodoListException(String message) {
	        super(message);
	    }

}
