package com.example.todolistreact.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.todolistreact.dto.TodoItemDTO;
import com.example.todolistreact.entity.TodoItem;
import com.example.todolistreact.exception.TodoListException;
import com.example.todolistreact.mapper.TodoItemMapper;
import com.example.todolistreact.repository.TodoItemRepository;

@Service
public class TodoListServiceImpl implements TodoListService {

	@Autowired
	private TodoItemRepository todoItemRepo;

	@Autowired
	private TodoItemMapper todoItemMapper;

	@Override
	public List<TodoItemDTO> getAllTodoItems() {
		List<TodoItem> todoItems = todoItemRepo.findAll();
		return todoItems.stream().map(todoItemMapper::toDTO).collect(Collectors.toList());
	}

	@Override
	public TodoItemDTO getTodoItemById(Long id) {
		TodoItem todoItem = todoItemRepo.findById(id)
				.orElseThrow(() -> new TodoListException("TodoItem not found with ID: " + id));
		return todoItemMapper.toDTO(todoItem);
	}

	@Override
	public TodoItemDTO createTodoItem(TodoItemDTO todoItemDTO) {
		TodoItem todoItem = todoItemMapper.toEntity(todoItemDTO);
		TodoItem savedTodoItem = todoItemRepo.save(todoItem);
		return todoItemMapper.toDTO(savedTodoItem);
	}

	@Override
	public TodoItemDTO updateTodoItem(Long id, TodoItemDTO todoItemDTO) {
		TodoItem existingTodoItem = todoItemRepo.findById(id)
				.orElseThrow(() -> new TodoListException("Cannot find TodoItem with ID: " + id));

		existingTodoItem.setTitle(todoItemDTO.getTitle());
		existingTodoItem.setDescription(todoItemDTO.getDescription());
		existingTodoItem.setCompleted(todoItemDTO.isCompleted());
		todoItemRepo.save(existingTodoItem);
		return todoItemMapper.toDTO(existingTodoItem);
	}

	@Override
	public boolean deleteTodoItem(Long id) {
		todoItemRepo.findById(id).orElseThrow(
				() -> new TodoListException("Delete operation failed. Cannot find TodoItem with ID: " + id));
		todoItemRepo.deleteById(id);
		return true;
	}
}