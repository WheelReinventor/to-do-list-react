package com.example.todolistreact.service;

import java.util.List;

import com.example.todolistreact.dto.TodoItemDTO;
import com.example.todolistreact.entity.TodoItem;

public interface TodoListService {
    List<TodoItemDTO> getAllTodoItems();
    
    TodoItemDTO getTodoItemById(Long id);
    
    TodoItemDTO createTodoItem(TodoItemDTO todoItemDTO);
    
    TodoItemDTO updateTodoItem(Long id, TodoItemDTO todoItemDTO);
    
    boolean deleteTodoItem(Long id);
}