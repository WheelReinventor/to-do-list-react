package com.example.todolistreact.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.todolistreact.dto.TodoItemDTO;
import com.example.todolistreact.service.TodoListService;

@RestController
@CrossOrigin(origins="http://localhost:3000")
@RequestMapping("/todoitems")
public class TodoListController {

	@Autowired
	private TodoListService todoListService;

	@GetMapping
	public ResponseEntity<List<TodoItemDTO>> getAllTodoItems() {
		List<TodoItemDTO> todoItems = todoListService.getAllTodoItems();
		return ResponseEntity.ok(todoItems);
	}

	@GetMapping("/{id}")
	public ResponseEntity<TodoItemDTO> getTodoItemById(@PathVariable("id") Long id) {
		TodoItemDTO todoItem = todoListService.getTodoItemById(id);
		return new ResponseEntity<TodoItemDTO>(todoItem, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<TodoItemDTO> createTodoItem(@RequestBody TodoItemDTO todoItem) {
		TodoItemDTO savedTodoItem = todoListService.createTodoItem(todoItem);
		return new ResponseEntity<TodoItemDTO>(savedTodoItem, HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<TodoItemDTO> updateTodoItem(@PathVariable("id") Long id, @RequestBody TodoItemDTO todoItem) {
		TodoItemDTO updatedTodoItem = todoListService.updateTodoItem(id, todoItem);
		return new ResponseEntity<TodoItemDTO>(updatedTodoItem, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteTodoItem(@PathVariable("id") Long id) {
		boolean deleted = todoListService.deleteTodoItem(id);
		return new ResponseEntity<Boolean>(deleted, HttpStatus.OK);
	}
}
